import { Component, OnInit } from '@angular/core';
import { RestService } from '../rest.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  loginData = {
    username: 'brian.hahn@alliedbuilding.com', 
    password: 'ALLied1234',
    siteId: 'homeSite'
  }
  constructor(public rest:RestService, private route: ActivatedRoute, private router: Router) { }

  ngOnInit() {
  }

  loginTest() {
    this.rest.login(this.loginData).subscribe((results) =>
      {
        console.log(results);
        this.router.navigate(['/home']);
      }, (err) => {
        console.log(err);
      })
  }
}
